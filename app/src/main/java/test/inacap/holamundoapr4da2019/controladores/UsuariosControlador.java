package test.inacap.holamundoapr4da2019.controladores;

import android.content.ContentValues;
import android.content.Context;

import test.inacap.holamundoapr4da2019.modelos.MainDBContract;
import test.inacap.holamundoapr4da2019.modelos.UsuariosModelo;

public class UsuariosControlador {
    private UsuariosModelo capaModelo;

    public UsuariosControlador(Context contexto){

        // Abrimos la conexion con la capa modelo, encargada de BD
        this.capaModelo = new UsuariosModelo(contexto);
    }

    /**
     *
     * @param username
     * @param password
     *
     * Este metodo toma los datos desde la capa vista, los procesa, transformando
     * los datos para que sean guardados por la base de datos
     */
    public void crearUsuario(String username, String password){

        // Instanciamos un objeto del tipo ContentValues
        // que nos permite agregar atributos clave/valor

        ContentValues datosUsuario = new ContentValues();

        // El nombre de la clave debe coincidir con el nombre de la columna
        datosUsuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, username);
        datosUsuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, password);

        // Enviamos los datos a la capa modelo
        this.capaModelo.crearUsuario(datosUsuario);
    }

    public boolean procesarLogin(String username, String password){
        // TODO: Construir la logica para ver si la persona inicio sesion correctamente
        return false;
    }
}
